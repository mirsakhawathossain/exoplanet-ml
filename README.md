# Exoplanet ML
This is a machine learning project for detecting exoplanets using transit survey based light curves.
# Machine Learning Algorithms

* Random Forest Classifier
* Naive Bayes
* LightGBM
* XGBoost
* AdaBoost
* Histogram Gradient Boosting
* Multi Layer Perceptron
* Neural Network
# Important Notebooks
* https://spacetelescope.github.io/notebooks/notebooks/MAST/Kepler/Kepler_Lightcurve/kepler_lightcurve.html
# Important Links
* https://www.rasgoml.com/feature-engineering-tutorials/how-to-create-time-series-features-with-tsfresh
* https://exoplanetarchive.ipac.caltech.edu/docs/acknowledge.html
* https://exoplanetarchive.ipac.caltech.edu/docs/doi.html
* https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=kep_conf_names
* https://exoplanetarchive.ipac.caltech.edu/docs/table-redirect.html

# Dimensionality reduction
* https://www.kaggle.com/code/samuelcortinhas/intro-to-pca-t-sne-umap
* https://plotly.com/python/t-sne-and-umap-projections/
* https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.KernelPCA.html#sklearn.decomposition.KernelPCA
* https://pair-code.github.io/understanding-umap/
* https://umap-learn.readthedocs.io/en/latest/basic_usage.html

# TsFresh Feature selection
* https://tsfresh.readthedocs.io/en/latest/api/tsfresh.feature_selection.html
* https://blog.mindmeldwithminesh.com/tsfresh-feature-extraction-by-distributed-and-parallel-means-for-industrial-big-data-applications-d84e9704702f

# Scikit Learn Supervised Learning List and Description
* https://scikit-learn.org/stable/supervised_learning.html
# Gaussian Process
* https://scikit-learn.org/stable/modules/gaussian_process.html
* https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.GaussianProcessClassifier.html#sklearn.gaussian_process.GaussianProcessClassifier
* https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.kernels.Hyperparameter.html
* https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.kernels.ConstantKernel.html
* https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.kernels.RBF.html
* https://scikit-learn.org/stable/auto_examples/gaussian_process/plot_gpr_noisy.html#sphx-glr-auto-examples-gaussian-process-plot-gpr-noisy-py
* https://scikit-learn.org/stable/modules/gaussian_process.html#gp-kernels
# Scikit Learn Unsupervised Learning List and Description
* https://scikit-learn.org/stable/unsupervised_learning.html
# Dask Blog for Hyperband
* https://blog.dask.org/2019/09/30/dask-hyperparam-opt#appendix

# Low Variance
* https://www.analyticsvidhya.com/blog/2021/04/beginners-guide-to-low-variance-filter-and-its-implementation/

## Common pitfalls in the interpretation of coefficients of linear models
* https://scikit-learn.org/stable/auto_examples/inspection/plot_linear_model_coefficient_interpretation.html#sphx-glr-auto-examples-inspection-plot-linear-model-coefficient-interpretation-py

## Cross-validation: evaluating estimator performance
* https://scikit-learn.org/stable/modules/cross_validation.html#stratification

## Feature Engineering Techniques

* https://www.analyticsvidhya.com/blog/2020/10/feature-selection-techniques-in-machine-learning/
* https://www.javatpoint.com/feature-selection-techniques-in-machine-learning
* https://www.simplilearn.com/tutorials/machine-learning-tutorial/feature-selection-in-machine-learning
* https://machinelearningmastery.com/feature-selection-with-real-and-categorical-data/

## Hyperopt Hyperparameter Tuning

* http://hyperopt.github.io/hyperopt/getting-started/search_spaces/
* https://www.kaggle.com/code/fanvacoolt/tutorial-on-hyperopt/notebook

## Incremental Principal Component Analysis

* https://www.geeksforgeeks.org/principal-component-analysis-with-python/
* https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.IncrementalPCA.html#sklearn.decomposition.IncrementalPCA.partial_fit
* https://www.geeksforgeeks.org/python-variations-of-principal-component-analysis/
* https://www.tutorialspoint.com/how-to-perform-dimensionality-reduction-using-python-scikit-learn

## Neural Network

* https://machinelearningmastery.com/use-keras-deep-learning-models-scikit-learn-python/
* https://machinelearningmastery.com/grid-search-hyperparameters-deep-learning-models-python-keras/
* https://keras.io/api/layers/activations/
* https://keras.io/api/optimizers/
* https://faroit.com/keras-docs/1.0.6/activations/
* https://faroit.com/keras-docs/1.0.6/optimizers/
* https://machinelearningmastery.com/understand-the-dynamics-of-learning-rate-on-deep-learning-neural-networks/
* https://keras.io/api/layers/activations/
* https://adriangb.com/scikeras/stable/quickstart.html
* https://adriangb.com/scikeras/stable/notebooks/MLPClassifier_MLPRegressor.html
* https://adriangb.com/scikeras/stable/migration.html#default-arguments-in-build-fn-model

## Quantum Neural Net

* https://towardsdatascience.com/quantum-machine-learning-with-python-kernel-methods-and-neural-networks-d60738aa99e1
* https://www.whiteboxml.com/en/blog/quantum-machine-learning-from-zero-to-hero
* https://medium.com/@anjanakrishnan3100/qnn-a-beginners-overview-and-breakdown-of-code-d4f62a0947f2

## Scikit Learn Ploting
* https://scikit-learn.org/stable/auto_examples/miscellaneous/plot_display_object_visualization.html#sphx-glr-auto-examples-miscellaneous-plot-display-object-visualization-py
* https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html#sklearn.metrics.confusion_matrix
* https://www.scikit-yb.org/en/latest/matplotlib.html
* https://scikit-learn.org/stable/modules/generated/sklearn.metrics.ConfusionMatrixDisplay.html#sklearn.metrics.ConfusionMatrixDisplay

## Probability Calibration

* https://scikit-learn.org/stable/modules/calibration.html
* https://scikit-learn.org/stable/modules/generated/sklearn.calibration.CalibratedClassifierCV.html#sklearn.calibration.CalibratedClassifierCV
* https://machinelearningmastery.com/calibrated-classification-model-in-scikit-learn/
* https://scikit-learn.org/stable/modules/generated/sklearn.calibration.calibration_curve.html
* https://www.geeksforgeeks.org/probability-calibration-curve-in-scikit-learn/

## Technical Problem Soution and Miscellaneous Links

* https://github.com/WongKinYiu/yolov7/issues/1280
* https://github.com/search?q=repo%3Ascikit-optimize%2Fscikit-optimize%20np.int&type=issues
* https://github.com/scikit-optimize/scikit-optimize/issues/1138
* https://github.com/scikit-optimize/scikit-optimize/issues/1171
* https://machinelearningmastery.com/roc-curves-and-precision-recall-curves-for-classification-in-python/
* https://machinelearningmastery.com/calibrated-classification-model-in-scikit-learn/

![Exoplanet](/Images/Hr8799_orbit_hd.gif/)
